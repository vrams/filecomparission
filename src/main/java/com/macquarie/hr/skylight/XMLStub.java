package com.macquarie.hr.skylight;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.util.List;
import java.util.Map;

public class XMLStub {

    private static Logger logger = Logger.getLogger(XMLStub.class);
    private DocumentBuilderFactory documentBuilderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    private XPath xpath;
    private XPathExpression xPathExpression;
    private NodeList nodeList;

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    private String xml = "";

    public XMLStub(String xmlStub) {
        try {
            xml = xmlStub;
            documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            xpath = XPathFactory.newInstance().newXPath();
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(xml)));
            xpath.setNamespaceContext(new WDNamespaceResolver(document));
        } catch (Exception e) {
            logger.info("Unable to create XML Document instances");
            e.printStackTrace();
        }
    }

    public String createXMLStub(List<Map<String, String>> scenarios){
        try {

            for (int i = 0; i < scenarios.size(); i++) {
                String xpaths = scenarios.get(i).get("NodeXPath");
                String value = scenarios.get(i).get("NodeValue");

                Node node = (Node) xpath.compile(xpaths).evaluate(document, XPathConstants.NODE);
                node.setTextContent(value);
            }
            setXML();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return xml;
    }

    private void setXML() {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = null;
        try {
            transformer = tf.newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        StringWriter writer = new StringWriter();
        try {
            transformer.transform(new DOMSource(document), new StreamResult(writer));
        } catch (TransformerException e) {
            e.printStackTrace();
        }
        xml = writer.getBuffer().toString().replaceAll("\n|\r", "\n");
    }

    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

}
