package com.macquarie.hr.skylight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by rkurian on 15/02/2018.
 */
public class SkylightFileProcessor {
    private XMLParser xmlParser;
    private static Logger logger = LoggerFactory.getLogger(SkylightFileProcessor.class);

    public SkylightFileProcessor(){}

    public SkylightFileProcessor(XMLParser xmlParser){
        this.xmlParser = xmlParser;
    }

    public String getFileTemplate(String inputFileName) throws Exception{
        String fileContent = null;
        String fileName = "target/test-classes/Templates/" + inputFileName ;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(fileName));
            StringBuilder sb = new StringBuilder();
            String line = reader.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = reader.readLine();
            }
            fileContent = sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("*** Exception while reading file template ---"+e.getMessage());
        } finally {
            try {
                if(reader != null){
                    reader.close();
                }

            } catch (IOException e) {
                e.printStackTrace();
                logger.info("****  Exeception  while closing Buffered reader ----"+e.getMessage());
            }
        }
        return fileContent;
    }

    public Document getExceptionsDOM(String inputPayload) {
        Document doc = xmlParser.xmlToNode(inputPayload);
        return doc;
    }

}
