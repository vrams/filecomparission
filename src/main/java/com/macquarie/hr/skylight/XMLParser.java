package com.macquarie.hr.skylight;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashMap;

/**
 * Created by vthaduri on 18/01/2017.
 */
public class XMLParser {
    private static Logger logger = Logger.getLogger(XMLParser.class);
    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder documentBuilder;
    private Document document;
    private XPath xpath;
    private PropertiesConfiguration pc;
    private NodeList nodeList;
    private Node node;
    private String namespace;

    public XMLParser() {
        try {
            dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setNamespaceAware(true);
            documentBuilder = dbFactory.newDocumentBuilder();
            xpath = XPathFactory.newInstance().newXPath();
            //pc = new PropertiesConfiguration("objectRepository.properties");
        } catch (Exception e) {
            logger.error("Unable to find Properties file");
            e.printStackTrace();
        }
    }

    public String getElement(String response, String xpath) throws Exception {
        return parseResponse(response, xpath).item(0).getFirstChild().getNodeValue();
    }

    /*
     * Get the node count of WD XML.
     */
    public int getNodeCount(String response, String xpath) throws Exception {
        return parseResponse(response, xpath).getLength();
    }

    //Parses the SOAP response and returns the desired value from the supplied xpath expression
    public NodeList parseResponse(String response, String xpathExpression) throws Exception {
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            String prefix = document.getDocumentElement().getPrefix();
            xpath.setNamespaceContext(new WDNamespaceResolver(document));
            XPathExpression expression = xpath.compile(xpathExpression);
            nodeList = (NodeList)expression.evaluate(document, XPathConstants.NODESET);
            if (nodeList.getLength() == 0 || nodeList == null) {
                logger.info("Error in Xpath Expression. Xpath doesn't exist");
            }
        } catch (NullPointerException e) {
            throw new Exception("XML Response is Empty");
        } catch (XPathExpressionException e) {
            throw new Exception("Invalid Xpath or Xpath doesn't exist");
        } catch (IOException e) {
            throw new Exception("Invalid XML Response or unable to parse the XML Document");
        } catch (SAXException e) {
            throw new Exception("XML Document parsing exception");
        }
        return nodeList;
    }

    /**
     *
     * @param response
     * @param xpathExpression
     * @return
     * @throws Exception
     */
    public String getNodeElement(String response, String xpathExpression) throws Exception {
        String element = "";
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            node = (Node) xpath.evaluate(xpathExpression, document, XPathConstants.NODE);
            if (node == null)
                element = "NULL";
            else
                element = node.getFirstChild().getNodeValue();

        } catch (NullPointerException e) {
            throw new Exception("XML Response is Empty");
        } catch (XPathExpressionException e) {
            throw new Exception("Invalid Xpath or Xpath doesn't exist");
        } catch (IOException e) {
            throw new Exception("Invalid XML Response or unable to parse the XML Document");
        } catch (SAXException e) {
            throw new Exception("XML Document parsing exception");
        }
        return element;
    }

    public HashMap<String,String> getNode(String response, String xpathExpression, String tagReference, String tagValue) throws Exception {
        HashMap<String,String> values = new HashMap<String, String>();
        try {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(response)));
            document.getDocumentElement().normalize();
            NodeList nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(document, XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    values.put(element.getElementsByTagName(tagReference).item(0).getTextContent(),
                            element.getElementsByTagName(tagValue).item(0).getTextContent());
                }
            }
        } catch (Exception e) {
            throw new Exception("Invalid SOAP Response/Xpath Expression");
        }
        return values;
    }
    public Document xmlToNode(String xmlString)
    {
        try
        {
            document = documentBuilder.parse(new org.xml.sax.InputSource(new StringReader(xmlString)));
            document.getDocumentElement().normalize();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return document;
    }

    public String getStringFromDocument(Document document)
    {
        try
        {
            //TODO - clone node does not work for xml element with prefixed namespace eg <ws:worker>
            // Commeting the following block
           /* Node root = document.getDocumentElement();
            NodeList rootChildren = root.getChildNodes();
            Element newRoot = document.createElement(root.getNodeName());
            for (int i = 0; i < rootChildren.getLength(); i++) {
                newRoot.appendChild(rootChildren.item(i).cloneNode(true));
            }
            document.replaceChild(newRoot, root);*/

            DOMSource domSource = new DOMSource(document);
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            transformer.transform(domSource, result);
            return writer.toString();
        }
        catch(TransformerException ex)
        {
            ex.printStackTrace();
            return null;
        }
    }
    public Document cleanNameSpace(Document doc) {

        NodeList list = doc.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            removeNamSpace(list.item(i), "");
        }

        return doc;
    }
    private void removeNamSpace(Node node, String nameSpaceURI) {

        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Document ownerDoc = node.getOwnerDocument();
            NamedNodeMap map = node.getAttributes();
            Node n;
            //for (!(0==map.getLength())) {
            while(map.getLength()!= 0){
                n = map.item(0);
                map.removeNamedItemNS(n.getNamespaceURI(), n.getLocalName());
            }
            ownerDoc.renameNode(node, nameSpaceURI, node.getLocalName());
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            removeNamSpace(list.item(i), nameSpaceURI);
        }
    }

}
