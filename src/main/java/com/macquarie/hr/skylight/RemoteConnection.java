package com.macquarie.hr.skylight;

import com.jcraft.jsch.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;


/**
 * Created by rkurian on 19/12/2017.
 */
public class RemoteConnection {
    private static Logger logger = LoggerFactory.getLogger(RemoteConnection.class);
    //SSH Connection class variables
    private JSch jsch;
    private Session jschSession;
    //MuleSoft Remote host connection variables
    private String hostName;
    private String userName;
    private String userDir;
    private int port;
    protected String certificatePath;


    private int maxWaitTime = 180000;
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserDir(String userDir) {
        this.userDir = userDir;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setCertificatePath(String certificatePath) {
        this.certificatePath = certificatePath;
    }


    /***************************************************************************
     Creates JSCH Channel session - Initiates Connection to the remote server
     ***************************************************************************/
    public void createSession()
    {
        jsch = new JSch();
        try {
            jsch.addIdentity(certificatePath.toString());
            jschSession = jsch.getSession(userName, hostName, port);
            jschSession.setConfig("PreferredAuthentications", "publickey,keyboard-interactive,password");
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            jschSession.setConfig(config);
            jschSession.connect();
            logger.info("** Connected to Host => " + hostName + " with Username => " + userName +" on Port => " + port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*********************************************************************************
     Destroys JSCH Channel session - This method is to destroy/disconnect the session
     **********************************************************************************/
    public void destroySession() {
        try {
            if (jschSession == null) {
                throw new JSchException("*** JSch session is null");
            }
            jschSession.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /************************************************************************
     Copies files to from windows or linux to local test exection directory
     ************************************************************************/
    public int copyFilesToLocal(String sourceDirectory, String destinationDirectory, String fileName) throws JSchException, SftpException {
        if (jschSession == null) {
            throw new JSchException("JSch Session is null");
        }
        logger.info("Copying file => " + fileName + " from: " + sourceDirectory + " to  Destination =>" + destinationDirectory);
        Channel sshChannel = null;
        ChannelSftp channelSftp = null;

        sshChannel = jschSession.openChannel("sftp");
        channelSftp = (ChannelSftp) sshChannel;
        sshChannel.connect();

        logger.info("Channel connection established successfully");
        channelSftp.cd(sourceDirectory);
        logger.info("Changed directory to => " + sourceDirectory);

        Vector<ChannelSftp.LsEntry> list = channelSftp.ls(fileName);
        for (ChannelSftp.LsEntry entry : list) {
            logger.info("File copied to local directory => " + entry.getFilename());
            channelSftp.get(entry.getFilename(), destinationDirectory + entry.getFilename());
        }
        return list.size();
    }
    /************************************************************************
     Copies files to from windows or linux to local test exection directory
     ************************************************************************/
    public int copyFilesToLocal(String sourceDirectory, String sourceFileName, String destinationDirectory, String destinationFileName) throws JSchException, SftpException {
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        logger.info("Copying file => " + sourceFileName + " from: " + sourceDirectory + " to  Destination =>" + destinationDirectory);
        Channel sshChannel = null;
        ChannelSftp channelSftp = null;

        sshChannel = jschSession.openChannel("sftp");
        channelSftp = (ChannelSftp) sshChannel;
        sshChannel.connect();

        logger.info("Channel connection established successfully");
        channelSftp.cd(sourceDirectory);
        logger.info("Changed directory to => " + sourceDirectory);

        Vector<ChannelSftp.LsEntry> list = channelSftp.ls(sourceFileName);
        for (ChannelSftp.LsEntry entry : list) {
            logger.info("File copied to local directory => " + entry.getFilename());
            channelSftp.get(entry.getFilename(), destinationDirectory + destinationFileName);
        }
        return list.size();
    }
    /**********************************************************************
     Copy files to Remote environment from local test executing directory
     *********************************************************************/
    public int copyFilesToRemote(String sourceFile, String destinationDirectory)
            throws JSchException, SftpException, FileNotFoundException {
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        String fileName ="";
        logger.info("*** Copying file => " + fileName + " from => " + sourceFile + " to  destination => " + destinationDirectory);
        int status = 0;
        Channel sshChannel = null;
        ChannelSftp channelSftp = null;
        sshChannel = jschSession.openChannel("sftp");
        channelSftp = (ChannelSftp) sshChannel;
        sshChannel.connect();
        logger.info("*** Channel connection established");
        channelSftp.put(sourceFile, destinationDirectory);
        logger.info("*** Changed directory to ---" + sourceFile);
        status = channelSftp.getExitStatus();
        channelSftp.disconnect();
        sshChannel.disconnect();
        logger.info("*** Executed the script; exit status  copyFilesToRemote() => " + status);
        return status;
    }

    /**********************************************************************
     Copy files to Remote environment from local test executing directory
     *********************************************************************/
    public int copyFileToRemote(String sourceDirectory, String destinationDirectory)
            throws JSchException, SftpException, FileNotFoundException {
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        String fileName ="";
        logger.info("*** Copying file => " + " from => " + sourceDirectory + " to  destination => " + destinationDirectory);
        int status = 0;
        Channel sshChannel = null;
        ChannelSftp channelSftp = null;
        sshChannel = jschSession.openChannel("sftp");
        channelSftp = (ChannelSftp) sshChannel;
        sshChannel.connect();
        logger.info("*** Channel connection established");
        channelSftp.put(sourceDirectory , destinationDirectory + fileName);
        logger.info("*** Changed directory to ---" + sourceDirectory);
        status = channelSftp.getExitStatus();
        channelSftp.disconnect();
        sshChannel.disconnect();
        logger.info("*** Executed the script; exit status  copyFilesToRemote() => " + status);
        return status;
    }


    /**********************************************************************
     Copy files to within the Remote linux environment environment
     *********************************************************************/
    public void copyFileWithinRemote(String sourceDir, String destinationDir, String fileName) throws JSchException, SftpException, FileNotFoundException {
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        ChannelExec channel = (ChannelExec) jschSession.openChannel("exec");
        channel.setCommand("cp " + sourceDir + fileName + " " + destinationDir + fileName);
        logger.info("*** Copying file from => " + sourceDir + " to destination => " + destinationDir);
        channel.connect();
        while (channel.isConnected()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int status = channel.getExitStatus();
        logger.info("*** Exit status in copy file within remote() => " + status);
        channel.disconnect();
    }

    /********************************************************************
     Delete file from Remote Linux server
     *********************************************************************/
    public void deleteFileFromRemote(String directory, String fileName, String fileType) throws JSchException, SftpException {
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        logger.info("**** Deleting file => " + fileName + " from the directory => " + directory);
        Channel sshChannel = null;
        ChannelSftp channelSftp = null;

        sshChannel = jschSession.openChannel("sftp");
        sshChannel.connect();
        channelSftp = (ChannelSftp) sshChannel;
        channelSftp.cd(directory);
        channelSftp.rm(fileName);
        logger.info("*** Deleted file => " + fileType + " from " + jschSession.getHost());
    }

    /********************************************************************
     Executes a shell script in given path
     *********************************************************************/
    public void executeScript(String script) throws JSchException, IOException {
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        logger.info("*** Executing the shell script => " + script);
        Channel channel = jschSession.openChannel("exec");
        ((ChannelExec) channel).setCommand(script);
        ((ChannelExec) channel).setErrStream(System.err);
        channel.connect();
        while (channel.isConnected()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        int status = channel.getExitStatus();
        channel.disconnect();
        logger.info("*** Executed the script; Exit status => " + status);
    }

    /********************************************************************
     Wait for the file name exists in a given directory
     *********************************************************************/
    public void waitForFileExist(String directory, String fileName)
            throws JSchException, SftpException, FileNotFoundException
    {

        int TimeExpired = 0;
        while (TimeExpired < maxWaitTime)
        {
            // If File Doesn't exist - We wait for few seconds
            // and check it again - Loop will break once the maxtiem is hit
            // or file exists in the given dir
            if(!sftpLSCommand(directory, fileName)) {
                SkylightUtil.waitFor(2000);
                TimeExpired  = TimeExpired +  2000;
            }
            else
            {
                logger.info("File Exists in the location: " + directory + fileName);
                break;
            }
        }
    }

    public void waitForFileDisappear(String directory, String fileName)
            throws JSchException, SftpException, FileNotFoundException
    {

        int TimeExpired = 0;
        while (TimeExpired < maxWaitTime)
        {
            // If File Does exist - We wait for few seconds
            // and check it again - Loop will break once the maxtiem is hit
            // or file is disappeared from the given dir
            if(sftpLSCommand(directory, fileName)) {
                SkylightUtil.waitFor(2000);
                TimeExpired  = TimeExpired +  2000;
            }
            else
            {
                logger.info("File disappeared in the location: " + directory + fileName);
                break;
            }
        }
    }

    private boolean sftpLSCommand(String directory, String fileName)
            throws JSchException, SftpException, FileNotFoundException
    {
        boolean fileExists = false;
        if (jschSession == null) {
            throw new JSchException("*** JSch session is null");
        }
        int status = 0;
        Channel sshChannel = null;
        ChannelSftp channelSftp = null;
        sshChannel = jschSession.openChannel("sftp");
        channelSftp = (ChannelSftp) sshChannel;
        sshChannel.connect();
        logger.info("*** Channel connection established");

        try {
            channelSftp.ls(directory + "/" + fileName );
            fileExists = true;
        } catch (SftpException e) {
            if (e.id == ChannelSftp.SSH_FX_NO_SUCH_FILE) {
                channelSftp.disconnect();
                sshChannel.disconnect();
                fileExists = false;
            }
//            logger.error("Unexpected exception during ls files on sftp: [{}:{}]", e.id, e.getMessage());
        }

        channelSftp.disconnect();
        sshChannel.disconnect();
        return fileExists;

    }

    /*public static void main(String args[]) throws Exception{
        String source = "/data/hrmuledv/mft_dev/WD214ExcInterface/error/";
        String destination = "C:\\rk123\\";
        RemoteConnection remoteConnection = null;
        int status = 0;
        int retries = 3;
        try{
            remoteConnection = new RemoteConnection("hrmuledv","usydasd10953",22,"target//test-classes//Certificates//hrmuledv.ppk");
            remoteConnection.createSession();
            status = remoteConnection.copyFilesToLocal(remoteConnection.jschSession,source,destination,"");
            if(status == 0){
                for(int i=0; i<3; i++){ //retry for few times.
                    if(status == 0){
                        Thread.sleep(2000); // sleep time should be a config param
                        status = remoteConnection.copyFilesToLocal(remoteConnection.jschSession,source,destination,"");
                    }
                }
            }
            if(status ==0 ){
                *//*
     *test failed; unable to copy workday file from mulesoft test folder
     *//*
                logger.info("*** Test failed ");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            remoteConnection.destroySession(remoteConnection.jschSession);
        }
    }*/
}
