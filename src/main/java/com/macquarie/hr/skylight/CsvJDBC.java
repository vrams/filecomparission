package com.macquarie.hr.skylight;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CsvJDBC {
    private static Connection connection = null;
    private PreparedStatement preparedStatement;
    private Statement statement;
    private static final String jdbcDriver = "org.relique.jdbc.csv.CsvDriver";

    public CsvJDBC() {
        try {
            Class.forName(jdbcDriver).newInstance();
            connection = DriverManager.getConnection("jdbc:relique:csv:C:\\Users\\vthaduri\\Projects\\skylight-automation\\SkylightCommons\\target\\classes\\TestData\\testData.csv");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CsvJDBC(String path) {
        try {
            Class.forName(jdbcDriver).newInstance();
            path = path.replace("/","\\").substring(1, path.length());
            connection = DriverManager.getConnection("jdbc:relique:csv:" + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printScenarios(String file) {
        try {
            Statement stmt = connection.createStatement();

            // Select the ID and NAME columns from sample.csv
            ResultSet results = stmt.executeQuery("SELECT * FROM " + file.split("\\.")[0] + " where scenario = 'SC01'");

            // Dump out the results to a CSV file with the same format
            // using CsvJdbc helper function
            ResultSetMetaData meta = results.getMetaData();

            while (results.next()) {

                for (int i = 0; i < meta.getColumnCount(); i++) {
                    System.out.println(meta.getColumnName(i + 1) + " " +
                            results.getString(i + 1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Map<String, String>> getScenario(String scenario) {
        List<Map<String, String>> scenarios = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            Statement statement = connection.createStatement();
            resultSet = statement.executeQuery("select * from testData");
            ResultSetMetaData meta = resultSet.getMetaData();

            while (resultSet.next()) {
                Map<String, String> sc = new HashMap<>();
                for (int i = 0; i < meta.getColumnCount(); i++) {
                    sc.put(meta.getColumnName(i + 1), resultSet.getString(i + 1));
                    System.out.println(meta.getColumnName(i + 1) + " " +
                            resultSet.getString(i + 1));
                }
                scenarios.add(sc);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


        return scenarios;

    }


    public static void main(String[] args) throws Exception {
        // Load the driver.
        Class.forName("org.relique.jdbc.csv.CsvDriver");

        // Create a connection. The first command line parameter is
        // the directory containing the .csv files.
        // A single connection is thread-safe for use by several threads.


        Connection conn = DriverManager.getConnection("jdbc:relique:csv:C:\\Users\\vthaduri\\Projects\\skylight-automation\\D41SailPoint\\target\\test-classes\\TestData");

        // Create a Statement object to execute the query with.
        // A Statement is not thread-safe.
        Statement stmt = conn.createStatement();

        // Select the ID and NAME columns from sample.csv
        ResultSet results = stmt.executeQuery("SELECT * FROM D35Exceptions where scenario = 'SC01'");

        // Dump out the results to a CSV file with the same format
        // using CsvJdbc helper function
        ResultSetMetaData meta = results.getMetaData();

        while (results.next()) {

            for (int i = 0; i < meta.getColumnCount(); i++) {
                System.out.println(meta.getColumnName(i + 1) + " " +
                        results.getString(i + 1));
            }
        }

        conn.close();
    }

    public List<Map<String, String>> getSingleScenario(List<Map<String, String>> dataTable, String testDatafile)
    {
        List<Map<String, String>> scenarios = new ArrayList<>();

        if (testDatafile.contains("."))
            testDatafile = testDatafile.split("\\.")[0];

        String query = "select InterfaceName, ScenarioNum, NodeXpath, NodeValue from " + testDatafile
                + " where ScenarioNum = '" + dataTable.get(0).get("ScenarioNum") + "'";
        try {
            statement = connection.createStatement();
            ResultSet results = statement.executeQuery(query);
            ResultSetMetaData meta = results.getMetaData();

            while (results.next()) {
                Map<String, String> scenario = new HashMap<>();
                for (int i = 0; i < meta.getColumnCount(); i++) {
                    scenario.put(meta.getColumnName(i + 1), results.getString(i + 1));
                }
                scenarios.add(scenario);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scenarios;
    }

}
