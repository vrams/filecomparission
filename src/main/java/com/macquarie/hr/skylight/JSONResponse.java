package com.macquarie.hr.skylight;

import java.util.Map;

/**
 * Hello world!
 *
 */
public class JSONResponse
{
    public String getResponse(String url)
    {
        return new RESTAPI().getResponse(url);
    }

    public String postResponse(String url, Map<String,String> map)
    {
        return new RESTAPI().postResponse(url,map);
    }
}
