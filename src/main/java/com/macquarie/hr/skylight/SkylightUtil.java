package com.macquarie.hr.skylight;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.*;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * Created by rkurian on 3/08/2017.
 */
public class SkylightUtil {

    public static String generateRandomText(String prefix, int txtLength){

        Random random = new Random();
        String addStr = "";
        StringBuilder builder = new StringBuilder();
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        while (builder.length() < txtLength) {
            int index = (int) (random.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }
        if(prefix == null){
            addStr =  builder.toString();
        }else {
            addStr = prefix + "_" + builder.toString();
        }
        return addStr;
    }

    public static String generateRandomText(int txtLength) {
        Random random = new Random();
        String addStr = "";
        StringBuilder builder = new StringBuilder();
        String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        while (builder.length() < txtLength) {
            int index = (int) (random.nextFloat() * chars.length());
            builder.append(chars.charAt(index));
        }
        addStr = builder.toString();
        return addStr;
    }

    public static String generateRandomNumber(int maxRange) {
        Random random = new Random();
        int num = random.nextInt(maxRange);
        return String.valueOf(num);
    }

    public static String generateRandonDOB() {
        GregorianCalendar gc = new GregorianCalendar();
        String dayOfMonth = null;
        int year = randBetween(1940, 1990);
        gc.set(gc.YEAR, year);
        int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
        gc.set(gc.DAY_OF_YEAR, dayOfYear);
        int day = gc.get(gc.DAY_OF_MONTH);
        String dob = gc.get(gc.DAY_OF_MONTH)+"/"+(gc.get(gc.MONTH) + 1)+"/"+gc.get(gc.YEAR);
        return dob;
    }

    private static  int randBetween(int start, int end) {
        return start + (int) Math.round(Math.random() * (end - start));
    }

    public static String generatePhoneNumber() {
        Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1);
        int num2 = rand.nextInt(7431);
        int num3 = rand.nextInt(1000);

        DecimalFormat df4 = new DecimalFormat("0000");
        DecimalFormat df2 = new DecimalFormat("00");

        return df2.format(num1) + " " + df4.format(num2) + " " + df4.format(num3);
    }

    public static String generateEmail() {
        Random r = new Random();
        return Long.toString(Math.abs(r.nextLong()), 36) + "@macquarie.com";
    }

    public static String generateRandomNumber(){
        Random rnd = new Random();
        int number = 1 + rnd.nextInt(99999);
         return String.valueOf(number);
    }

    public static String formatDate(String dateValue, String dateFormat, String expectedFormat) {
        Date date = null;
        SimpleDateFormat formatter = null;
        String formattedDate = "";
        try {
            formatter = new SimpleDateFormat(dateFormat);
            date = formatter.parse(dateValue);
            formatter = new SimpleDateFormat(expectedFormat);
            formattedDate = formatter.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static void waitFor(long time){
        try{
            Thread.sleep(time);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static File[] getFileFromLocal(String directory , String wildCard){
        File dir = null;
        FileFilter fileFilter = null;
        File[] files = null;
        try{
            dir = new File(directory);
            fileFilter = new WildcardFileFilter(wildCard);
            files = dir.listFiles(fileFilter);
            if(files.length == 0){
                throw new Exception("*** Unable to find workday/mulesoft output in local directory");
            }
            for (int i = 0; i < files.length; i++) {
                System.out.println(files[i]);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return files;
    }

    public static String replaceAllQuotesAndWhiteSpace(String value){
        value =  value.replaceAll("^\"|\"$", "");
        return   value.replaceAll("\\s+","");
    }

    public static String replaceAllWhiteSpace(String value){
        return value.replaceAll("\\s+","");
    }

    public static void main(String args[])
    {
        System.out.println(SkylightUtil.getBaseFilename("/C:/Users/vthaduri/Projects/skylight-automation/D41SailPoint/target/mulesoft-files/WD_Exc_user_stub_20180312_143424.xml"));
    }

    public static  String getCurrentDate(String dateFormat){
        Date curDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(curDate);
    }

    public static String convertInputStreamToString(InputStream inputStream)
    {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(inputStream, writer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();
    }

    public static void saveFile(String path, String content)
    {
        try {
            File file = new File(path);
            file.getParentFile().mkdirs();
            FileWriter fw = new FileWriter( file.getAbsoluteFile( ) );
            BufferedWriter bw = new BufferedWriter( fw );
            bw.write( content );
            bw.close( );        } catch (Exception e) {
            e.printStackTrace();
        }
            
    }

    public static String getBaseFilename(String filePath)
    {
        return FilenameUtils.getName(filePath);
    }

}
