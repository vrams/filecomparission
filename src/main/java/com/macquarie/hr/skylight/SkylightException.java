package com.macquarie.hr.skylight;

public class SkylightException extends Exception {
    public SkylightException(String message)
    {
        super(message);
    }
}
