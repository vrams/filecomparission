package com.macquarie.hr.skylight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class RESTAPI {
    private static final Logger logger = LoggerFactory.getLogger(RESTAPI.class);
    /*Get response from API */
    public String getResponse(String url)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        logger.info("JSON Response: " + responseEntity.toString());
        return responseEntity.toString();
    }
    /*POST response to API */
    public String postResponse(String url, Map<String, String> map)
    {
        map = new HashMap<String, String>();
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url,map, String.class);
        logger.info("JSON Response: " + responseEntity.toString());
        return responseEntity.toString();
    }
}
