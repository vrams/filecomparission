@D35Exceptions @WD214ExcInterface
Feature: D35Exceptions WD214ExcInterface - Automated Tests
  In Order to test Workday Exceptions Interface (D35)
  As a Macquarie Mulesoft user
  I want to verify processing of files in Mulesoft
  MFT: Macquarie File Transfer

  Background:
    Given I set Interface name as "WD214ExcInterface"

  @smoke @regression @positive
  Scenario Outline: Run successfull scenario for Exceptions
    Given I create Stub from given template using CSV file
      | ScenarioNum   | CSVDataFile   | Template   |
      | <ScenarioNum> | <CSVDataFile> | <Template> |
    Then I transfer and encrypt the Stub file on Mulesoft
    And I copy the stub file to "landing" directory
    And I wait for the file to disappear from "landing" directory
    And I verify the file in "processed" directory
    And I verify the file contents "decrypt"

    Examples:
      | ScenarioNum | CSVDataFile       | Template                 |
      | SC001       | D35Exceptions.csv | WD_Exc_user_template.xml |