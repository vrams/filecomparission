@D006Benefits @WD334THUKInterface
Feature: D006Benefits WD334THUKInterface - Automated Tests
  In Order to test Workday Benefits Interface (D06)
  As a Macquarie Mulesoft user
  I want to verify processing of files in Mulesoft
  MFT: Macquarie File Transfer

  Background:
    Given I set Interface name as "WD334THUKInterface"

  @smoke @regression @positive
  Scenario Outline: Run successfull scenario for benefits
    Given I create Stub from given template using CSV file
      | ScenarioNum   | CSVDataFile   | Template   |
      | <ScenarioNum> | <CSVDataFile> | <Template> |
    Then I transfer and encrypt the Stub file on Mulesoft
    And I copy the stub file to "landing" directory
    And I wait for the file to disappear from "landing" directory
    And I verify the file in "processed" directory
    And I verify the file contents "decrypt"

    Examples:
      | ScenarioNum | CSVDataFile                 | Template                             |
      | SC001       | 334_ThomsonsUKInterface.csv | WD_ThompsonsUK_employee_template.xml |