package com.macquarie.hr.skylight;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;


/**
 * Created by vthaduri on 07/11/2017.
 */
public class StepDefinitions {
    private static Logger logger = LoggerFactory.getLogger(StepDefinitions.class);


    private PropertiesConfiguration properties;

    protected Container container;
    protected List<Map<String, String>> dataTable;
    private CsvJDBC csvJDBC;
    private String stubName;

    public StepDefinitions(Container container) {
        this.container = container;
        try {
            properties = new PropertiesConfiguration("Skylight.properties");

        } catch (Exception e) {
            logger.info("Unable to get the properties file");
            e.printStackTrace();
        }

    }

    @Given("^I set Interface name as \"([^\"]*)\"$")
    public void i_set_Interface_name_as(String interfaceName) throws Throwable {
        //Set interface name in the container
        container.setInterfaceName(interfaceName);
        this.container.setTestDataDir(this.getClass().getClassLoader().getResource("TestData").getPath() + "/" + container.getInterfaceName());
        //Get build directory to store Mulesoft files
        URL location = this.getClass().getProtectionDomain().getCodeSource().getLocation();
        this.container.setBuildOutputDir(location.getFile().replace("test-classes", "mulesoft-files") + container.getInterfaceName() + "/");
    }

    //Initialize Container Variables
    @Given("^I create Stub from given template using CSV file$")
    public void i_create_Stub_from_given_template_using_CSV_file(DataTable scenarios) {
        //Get the variables from Cucumber Data table
        dataTable = scenarios.asMaps(String.class, String.class);
        String templateName = dataTable.get(0).get("Template");
        String testDataFile = dataTable.get(0).get("CSVDataFile");
        //Create XML Stub
        createXMLStub(container, templateName, testDataFile);
    }

    @Then("^I transfer and encrypt the Stub file on Mulesoft$")
    public void i_transfer_and_encrypt_the_Stub_file_on_Mulesoft() throws Throwable {
        // Encrypt the Xml Stub File
        encryptStubFile(container);
    }

    @Then("^I copy the stub file to \"([^\"]*)\" directory$")
    public void i_copy_the_stub_file_to_inbound_directory(String directory) throws Throwable {
        container.remoteConnection.copyFileWithinRemote(container.getEncryptOutputDir(),
                container.getRootDir() + container.getInterfaceName() + "/" +directory + "/",
                SkylightUtil.getBaseFilename(stubName));
    }

    @Then("^I wait for the file to disappear from \"([^\"]*)\" directory$")
    public void i_wait_for_the_file_to_disappear_from_inbound_directory(String directory) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        container.remoteConnection.waitForFileDisappear(container.getRootDir()
                        + container.getInterfaceName() +"/" + directory + "/", SkylightUtil.getBaseFilename(stubName));

    }

    @Then("^I verify the file in \"([^\"]*)\" directory$")
    public void i_verify_the_file_in_directory(String directory) throws Throwable {
        container.remoteConnection.waitForFileExist(container.getRootDir()
                        + container.getInterfaceName() + "/" + directory + "/",
                SkylightUtil.getBaseFilename(stubName));

    }
    
    @Then("^I verify the file contents \"([^\"]*)\"$")
    public void i_verify_the_file_contents(String directory) throws Throwable {


    }

    public void createXMLStub(Container container, String templateName, String testDataFile) {

        //Get the template file as InputStream and convert it back to XML String
        InputStream is = this.getClass().getClassLoader().getResourceAsStream("Templates/" + container.interfaceName + "/"+ templateName);
        container.xml = SkylightUtil.convertInputStreamToString(is);
        //Create XMLStub and CSVJdbc variables
        csvJDBC = new CsvJDBC(container.testDataDir);
        XMLStub xmlStub = new XMLStub(container.xml);
        //Process your template to create stub file and save it in continer XML variable
        container.xml = xmlStub.createXMLStub(csvJDBC.getSingleScenario(dataTable, testDataFile));
        //Create dynamic stub filename with date and timestamp
        stubName = SkylightUtil.getCurrentDate("yyyyMMdd_HHmmss");
        stubName = templateName.replace("template", stubName);
        //Save the file in target Directory
        stubName = container.buildOutputDir + stubName;
        SkylightUtil.saveFile(stubName, container.xml);
    }

    public void encryptStubFile(Container container) throws Throwable {
        //Copy the File to Mulesoft test workday encryption directory
        container.remoteConnection.copyFilesToRemote(stubName, container.getEncryptInputDir());
        //Call the encrypt shell script within workday encryption directory
        container.remoteConnection.executeScript(container.getEncryptScript());
        //Call the CopyFunction of the directory
        String baseFileName = SkylightUtil.getBaseFilename(stubName);
        URL location = this.getClass().getProtectionDomain().getCodeSource().getLocation();
        //baseFileName = stubName.replace(".","_encrypted.");
        String newFilename = SkylightUtil.getBaseFilename(stubName.replace(".", "_encrypted."));
        container.remoteConnection.copyFilesToLocal(container.getEncryptOutputDir(), baseFileName, container.buildOutputDir, newFilename);

    }

}