package com.macquarie.hr.skylight;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        plugin = {"pretty", "html:target/cucumber-reports",
                "junit:target/test-reports.xml",
                "json:target/cucumber.json", "rerun:target/FailedScenarios.txt"}
                //,tags = "@benefits"

)
public class CukeRunner   {

}
