package com.macquarie.hr.skylight;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Container {

    private static Logger logger = LoggerFactory.getLogger(Container.class);

    //XML related variables
    protected XMLStub xmlStubFilePath;
    protected String xml;
    //Interface name and environment to run
    protected String interfaceName;
    protected String environment;
    //Test Data Directory & Test Output Directory
    protected String testDataDir;
    protected String buildOutputDir;

    //Remote host Mulesoft directory path
    protected String rootDir;
    protected String encryptScript;
    protected String encryptInputDir;
    protected String encryptOutputDir;
    protected String landingDir;
    protected String processedDir;
    protected String errorDir;
    protected String outboundDir;
    protected String decryptScript;
    protected String decryptOutputDir;


    //Properties configuration to fetch properties
    PropertiesConfiguration properties;
    //Remote Connection
    RemoteConnection remoteConnection = new RemoteConnection();


    public XMLStub getXmlStubFilePath() {
        return xmlStubFilePath;
    }

    public void setXmlStubFilePath(XMLStub xmlStubFilePath) {
        this.xmlStubFilePath = xmlStubFilePath;
    }

    public String getTestDataDir() {
        return testDataDir;
    }

    public void setTestDataDir(String testDataDir) {
        this.testDataDir = testDataDir;
    }

    public String getBuildOutputDir() {
        return buildOutputDir;
    }

    public void setBuildOutputDir(String buildOutputDir) {
        this.buildOutputDir = buildOutputDir;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        properties.setProperty("interfaceName",interfaceName);
        this.interfaceName = properties.getString("interfaceName");
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
    }

    public String getRootDir() {
        return rootDir;
    }

    public void setEncryptScript(String encryptScript) {
        this.encryptScript = encryptScript;
    }

    public String getEncryptScript() {
        return encryptScript;
    }

    public void setEncryptInputDir(String encryptInputDir) {
        this.encryptInputDir = encryptInputDir;
    }

    public String getEncryptInputDir() {
        return encryptInputDir;
    }

    public void setEncryptOutputDir(String encryptOutputDir) {
        this.encryptOutputDir = encryptOutputDir;
    }

    public String getEncryptOutputDir() {
        return encryptOutputDir;
    }

    public void setLandingDir(String landingDir) {
        this.landingDir = landingDir;
    }

    public void setProcessedDir(String processedDir) {
        this.processedDir = processedDir;
    }

    public void setErrorDir(String errorDir) {
        this.errorDir = errorDir;
    }

    public void setOutboundDir(String outboundDir) {
        this.outboundDir = outboundDir;
    }

    public void setDecryptScript(String decryptScript) {
        this.decryptScript = decryptScript;
    }

    public void setDecryptOutputDir(String decryptOutputDir) {
        this.decryptOutputDir = decryptOutputDir;
    }


    @Before
    public void setUp() {
        try {
            properties = new PropertiesConfiguration("Skylight.properties");
        } catch (Exception e) {
            logger.info("Unable to find the properties file");
            e.printStackTrace();
        }
        setEnvironment();
        setRemoteConnectionCredentials();
    }

    private void setEnvironment() {
        //Check which environment whether the user is running
        if ((System.getProperty("environment") == null) | System.getProperty("env") == null)
            environment = "dev";
        else
            environment = "test";
        //As per the environment assign Mulesoft directories using variable interpolation
        if (environment.equalsIgnoreCase("dev")) {
            properties.setProperty("hostName", properties.getString("devHostName"));
            properties.setProperty("userName", properties.getString("devUserName"));
            properties.setProperty("userDir", properties.getString("devUserDir"));
            properties.setProperty("port", properties.getString("devPort"));
        } else if (environment.equalsIgnoreCase("test")) {
            properties.setProperty("hostName", properties.getString("testHostName"));
            properties.setProperty("userName", properties.getString("testUserName"));
            properties.setProperty("userDir", properties.getString("testUserDir"));
            properties.setProperty("port", properties.getString("testPort"));
        }
    }

    private void setRemoteConnectionCredentials()
    {
        //Set remote host connection variables
        String certificatePath = this.getClass().getClassLoader().getResource("Certificates/hrmuledv.ppk").getFile();
        remoteConnection.setHostName(properties.getString("hostName"));
        remoteConnection.setUserName(properties.getString("userName"));
        remoteConnection.setUserDir(properties.getString("userDir"));
        remoteConnection.setPort(properties.getInt("port"));
        remoteConnection.setCertificatePath(certificatePath);
        remoteConnection.createSession();
        //Set root directory
        setRootDir(properties.getString("rootDir"));
        //Set encrypt directory
        setEncryptScript(properties.getString("encryptScript"));
        setEncryptInputDir(properties.getString("encryptInputDir"));
        setEncryptOutputDir(properties.getString("encryptOutputDir"));
        //Set Mulesoft directories
        setLandingDir(properties.getString("landingDir"));
        setProcessedDir(properties.getString("processedDir"));
        setErrorDir(properties.getString("errorDir"));
        setOutboundDir(properties.getString("outboundDir"));
        //Set decrypt directory
        setDecryptScript(properties.getString("decryptScript"));
        setDecryptOutputDir(properties.getString("decryptOutputDir"));
        //Create JSCH Session using the variables
    }

    @After
    public void TearDown() {
        remoteConnection.destroySession();
    }
}
